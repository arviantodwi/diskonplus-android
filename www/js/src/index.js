// var app = {
//     // Application Constructor
//     initialize: function() {
//         this.bindEvents();
//     },
//     // Bind Event Listeners
//     //
//     // Bind any events that are required on startup. Common events are:
//     // 'load', 'deviceready', 'offline', and 'online'.
//     bindEvents: function() {
//       document.addEventListener('deviceready', this.onDeviceReady, false);
//     },
//     // deviceready Event Handler
//     //
//     // The scope of 'this' is the event. In order to call the 'receivedEvent'
//     // function, we must explicitly call 'app.receivedEvent(...);'
//     onDeviceReady: function() {
//       // app.receivedEvent('deviceready');
//       window.youtube.init("AIzaSyBM6tRNjiY_BzdBcq1ycZPKqMBmF9GlzHg");
//     },
//     // Update DOM on a Received Event
//     // receivedEvent: function(id) {
//       // var parentElement = document.getElementById(id);
//       // var listeningElement = parentElement.querySelector('.listening');
//       // var receivedElement = parentElement.querySelector('.received');

//       // listeningElement.setAttribute('style', 'display:none;');
//       // receivedElement.setAttribute('style', 'display:block;');

//       // console.log('Received Event: ' + id);
//     // }
// };

// app.initialize();
var landingSlider, grabSlider, carousel;

$(document).ready(function() { 
  // jQuery is properly loaded at this point
  
  // Attach FastClick event to document
  FastClick.attach(document.body);
  
  landingSlider = new Swiper('.swiper-container', {
    direction: 'horizontal',
    loop: true,
    pagination: '.swiper-pagination',
    autoplay: 3200
  });
  
  grabSlider = new Swiper('.grab-swiper-container', {
      slidesPerView: 2.33,
      centeredSlides: false,
      spaceBetween: 16,
      grabCursor: false
  });
  
  // Proceed to bind the Cordova's deviceready event
  $(document).bind("deviceready", function() {
    // Now Cordova is loaded
    // its great JS API can be used to access low-level
    // features as accelerometer, contacts and so on
    
    // Attach Youtube API key
    window.youtube.init("AIzaSyBM6tRNjiY_BzdBcq1ycZPKqMBmF9GlzHg");
    // Override default HTML alert with native dialog
    if(navigator.notification) {
      window.prompt = function(message) {
        navigator.notification.prompt(
          message,
          function(){},
          'Redeem Voucher',
          ['Redeem', 'Cancel'],
          null
        );
      };
    }
  });
  
  $('.voucher').on('click', function() {
    prompt('Enter the voucher unique code');
  });
  
  $('div[data-trigger-merchant=true]').on('click', function() {
    $('.view[data-page=merchant]').show();
    carousel = new Swiper('.carousel-container', {
      scrollbar: '.indicator',
      scrollbarHide: false,
      slidesPerView: 1,
      centeredSlides: false,
      spaceBetween: 0,
      onTouchStart: function(carousel) {
        if(carousel.activeIndex === 1) { carousel.lockSwipeToNext(); }
        else if(carousel.activeIndex === 0) { carousel.lockSwipeToPrev(); }
      },
      onSlideChangeEnd: function(carousel) {
        $('.carousel-trigger > a[data-item-index='+carousel.activeIndex+']').addClass('active').siblings().removeClass('active');
      },
      onProgress: function(carousel, progress) {
        if(progress > 0 && progress < 1) { carousel.unlockSwipes(); }
      }
    });
    $('.view[data-page=landing]').hide();
  });
  
  $('button[data-back=landing]').on('click', function() {
    $('.view[data-page=landing]').show();
    $('.view[data-page=merchant]').hide();
  });
});

// (function(jq) {
//   jq('.carousel-trigger').find('a').on('touchstart touchend', function(ev) {
//     ev.preventDefault();
//     ev.stopImmediatePropagation();
//     jq(this).addClass('active').siblings().removeClass('active');
//     carousel.slideTo( parseInt(jq(this).data('item-index')) );
//   });
// })(jQuery);